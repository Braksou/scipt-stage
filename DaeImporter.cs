using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using UnityEditor;

public class DaeImporter
{

    private struct meshStruct
    {
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] uv;
        public Vector2[] uv1;
        public Vector2[] uv2;
        public int[] triangles;
        public int[] faceVerts;
        public int[] faceUVs;
        public Vector3[] faceData;
        public string name;
        public string fileName;
    }

    private struct bonesStruct
    {
        public BoneWeight[] weights;
        public Transform[] bones;
        public Matrix4x4[] bindPoses;
        public string[] bonesName;
    }
    bonesStruct newBones;

    // Use this for initialization
    public List<Mesh> ImportFile(string filePath, GameObject go)
    {
        List<Mesh> res = new List<Mesh>();
        List<meshStruct> newMesh = createMeshStruct(filePath);
        newBones = createBoneStruct(filePath, go);
        populateMeshStruct(ref newMesh);
        foreach (meshStruct item in newMesh)
        {
            /*Vector3[] newVerts = new Vector3[item.vertices.Length];
            Vector2[] newUVs = new Vector2[item.vertices.Length];
            Vector3[] newNormals = new Vector3[item.normals.Length];

            Debug.Log(item.faceData.Length);
            Debug.Log(item.normals.Length);
            Debug.Log(item.vertices.Length);
            Debug.Log(item.triangles.Length);
            Debug.Log(item.uv.Length);

            Mesh mesh = new Mesh();
            if(item.triangles.Length >= (Int16.MaxValue + Int16.MinValue*-1))
            {
                mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            }
            
            mesh.vertices = item.vertices;
            mesh.normals = item.normals;
            mesh.triangles = item.triangles;
            for (int i = 0; i < item.faceData.Length; i++)
            {
                newUVs[item.triangles[i]] = item.uv[(int)item.faceData[i].z];
            }
            Debug.Log(GameObject.FindObjectOfType<InfoMesh>().GetUV().Length);
            mesh.uv = newUVs;
            mesh.boneWeights = newBones.weights;
            mesh.RecalculateBounds();
            mesh.Optimize();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\master\Desktop\UV.txt"))
            {
                foreach (Vector2 line in newUVs)
                {
                    file.WriteLine(line);
                }
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\master\Desktop\Vertices.txt"))
            {
                foreach (Vector3 line in item.vertices)
                {
                    file.WriteLine(line);
                }
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\master\Desktop\Triangles.txt"))
            {
                foreach (int line in item.triangles)
                {
                    file.WriteLine(line);
                }
            }*/
            Vector3[] newVerts = new Vector3[item.vertices.Length];
            Vector2[] newUVs = new Vector2[item.uv.Length];
            Vector3[] newNormals = new Vector3[item.normals.Length];
            int vertcount = item.vertices.Length;
            int normalsCount = item.normals.Length;
            int newOne = 0;
            int exist = 0;
            int existNew = 0;
            int max = 0;
            Debug.Log(item.triangles.Length);
            Debug.Log(item.uv.Length);
            Dictionary<int, List<Vector2>> dejaVu = new Dictionary<int, List<Vector2>>();
            Dictionary<int, List<Vector2>> border = new Dictionary<int, List<Vector2>>();
            for (int i = 0; i < item.faceData.Length; i++)
            {
                newUVs[item.triangles[i]] = item.uv[(int)item.faceData[i].z];
            }
            List<int> vector2s = new List<int>();
            List<Vector2> content = new List<Vector2>();
            for (int i = 0; i < item.faceData.Length; i++)
            {
                dejaVu.TryGetValue(item.triangles[i], out content);
                if(content == null)
                {
                    content = new List<Vector2>();
                    content.Add(item.uv[(int)item.faceData[i].z]);
                    dejaVu.Add(item.triangles[i], content);
                    newOne++;
                    newNormals[item.triangles[i]] = item.normals[(int)item.faceData[i].y];
                    newVerts[item.triangles[i]] = item.vertices[(int)item.faceData[i].x];
                    newUVs[item.triangles[i]] = item.uv[(int)item.faceData[i].z];
                }
                else if(content.Contains(item.uv[(int)item.faceData[i].z])){
                    exist++;
                }
                else
                {
                    existNew++;
                    content.Add(item.uv[(int)item.faceData[i].z]);
                    dejaVu.Remove(item.triangles[i]);
                    dejaVu.Add(item.triangles[i], content);
                    border.Remove(item.triangles[i]);
                    border.Add(item.triangles[i], content);
                    vector2s.Add(item.triangles[i]);
                }
            }
            Debug.Log(newOne + " " + exist + " " + existNew + " " + max);
            int k = 0;
            /*foreach (var v in border)
            {
                //newUVs[v.Key] = new Vector2(v.Value[1].x + 0.1f, v.Value[0].y - 0.1f);
                Debug.Log(v.Key);
            }
            k = 0;
            foreach (var v in border)
            {
                if (k % 2 == 0)
                {
                    newUVs[v.Key] = v.Value[1];
                    Debug.Log(v.Key);
                    //newVerts[v.Key] = new Vector3(newVerts[v.Key].x, newVerts[v.Key].y, newVerts[v.Key].z - 0.5f);
                }
                k++;
            }
            List<Vector2> newList = new List<Vector2>();
            border.TryGetValue(1754, out newList);
            newUVs[1754] = new Vector2(newList[1].x + 0.1f, newList[0].y-0.1f);
            newVerts[1754] = new Vector3(newVerts[1754].x, newVerts[1754].y, newVerts[1754].z);*/
            Mesh mesh = new Mesh();
            if (item.triangles.Length >= (Int16.MaxValue + Int16.MinValue * -1))
            {
                mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            }
            mesh.vertices = newVerts;
            mesh.normals = newNormals;
            mesh.triangles = item.triangles;
            mesh.uv = newUVs;
            mesh.RecalculateBounds();
            mesh.boneWeights = newBones.weights;
            res.Add(mesh);
        }
        return res;
    }
    
    public Transform[] GetTransforms()
    {
        return newBones.bones;
    }

    public Matrix4x4[] GetMatrix()
    {
        return newBones.bindPoses;
    }

    private static List<meshStruct> createMeshStruct(string filename)
    {
        meshStruct mesh;
        mesh = new meshStruct();
        List<meshStruct> listMeshes = new List<meshStruct>();
        int count = 0;
        int face = 0;
        int triangles = 0;
        StreamReader stream = File.OpenText(filename);
        string entireText = stream.ReadToEnd();
        stream.Close();
        bool onlyOnce = true;
        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();
            char[] splitIdentifier = { ' ' };
            string[] brokenString;
            while (!currentText.StartsWith("<library_geometries>"))
            {
                currentText = reader.ReadLine();
                
            }
            while(!currentText.StartsWith("</library_geometries>"))
            {
                currentText = currentText.Trim();                           //Trim the current line
                brokenString = currentText.Split(splitIdentifier, 50);
                switch (brokenString[0])
                {
                    case "<mesh>":
                        mesh = new meshStruct();
                        mesh.fileName = filename;
                        break;
                    case "<source":
                        currentText = reader.ReadLine();
                        currentText = currentText.Trim();
                        string[] brokenBString;
                        brokenBString = currentText.Split(splitIdentifier);
                        if(count == 0)
                        {
                            mesh.vertices = new Vector3[(brokenBString.Length -2) / 3];
                            count++;
                        }
                        else if(count == 1)
                        {
                            mesh.normals = new Vector3[(brokenBString.Length - 2) / 3];
                            count++;
                        }
                        else
                        {
                            mesh.uv = new Vector2[(brokenBString.Length - 2)/2];
                        }
                        
                        break;
                    case "<triangles":
                        if (onlyOnce)
                        {
                            currentText = reader.ReadLine();
                            currentText = currentText.Trim();
                            string[] brokenBBString;
                            brokenBBString = currentText.Split(splitIdentifier);
                            while (!brokenBBString[0].Contains("<p>"))
                            {
                                currentText = reader.ReadLine();
                                currentText = currentText.Trim();
                                brokenBBString = currentText.Split(splitIdentifier);
                            }
                            currentText = currentText.Trim();
                            brokenBBString = currentText.Split(splitIdentifier);
                            for (int i = 0; i < brokenBBString.Length; i = i + 3)
                            {
                                face = face + 3;
                                triangles++;
                            }
                            onlyOnce = false;
                        } 
                        break;
                    case "</mesh>":
                        mesh.faceData = new Vector3[face / 3];
                        if(triangles%3 == 0)
                        {
                            mesh.triangles = new int[triangles];
                        }
                        else
                        {
                            mesh.triangles = new int[triangles +  3 - triangles % 3];
                        }
                        listMeshes.Add(mesh);
                        count = 0;
                        face = 0;
                        triangles = 0;
                        break;
                    default :
                        break;
                }
                currentText = reader.ReadLine();
            }
        }
        return listMeshes;
    }

    private static void populateMeshStruct(ref List<meshStruct> mesh)
    {
        bool onlyOnce = true;
        StreamReader stream = File.OpenText(mesh[0].fileName);
        string entireText = stream.ReadToEnd();
        stream.Close();
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.CultureInfo("en-US", false).NumberFormat;
        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();

            char[] splitIdentifier = { ' ', '>', '<','/' };
            char[] splitIdentifier2 = { '>' };
            string[] brokenString;
            string[] brokenBrokenString;
            int f = 0;
            int f2 = 0;
            int v = 0;
            int vn = 0;
            int vt = 0;
            int id = -1;
            int count = 0;
            List<int> intArray = new List<int>();
            while (!currentText.StartsWith("<library_geometries>"))
            {
                currentText = reader.ReadLine();
            }
            while (!currentText.StartsWith("</library_geometries>"))
            {
                currentText = currentText.Trim();                           //Trim the current line
                brokenString = currentText.Split(splitIdentifier, 50);
                switch (brokenString[1])
                {
                    case "mesh":
                        if(count == 0)
                        {
                            id++;
                        }
                        else
                        {
                            f = 0;
                            f2 = 0;
                            v = 0;
                            vn = 0;
                            vt = 0;
                            count = 0;
                        }
                        break;
                    case "source":
                        currentText = reader.ReadLine();
                        currentText = currentText.Trim();
                        string[] brokenBString;
                        brokenBString = currentText.Split(splitIdentifier);
                        if (!brokenBString[3].Equals("count=\"0\""))
                        {
                            if (count == 0)
                            {
                                for (int i = 4; i < brokenBString.Length - 4; i = i + 3)
                                {
                                    brokenBString[i] = brokenBString[i].Replace('.', ',');
                                    brokenBString[i + 1] = brokenBString[i + 1].Replace('.', ',');
                                    brokenBString[i + 2] = brokenBString[i + 2].Replace('.', ',');

                                    mesh[id].vertices[v] = new Vector3(float.Parse(brokenBString[i]), float.Parse(brokenBString[i + 1]),
                                                            float.Parse(brokenBString[i + 2]));
                                    v++;
                                }
                                count++;
                            }
                            else if (count == 1)
                            {
                                for (int i = 4; i < brokenBString.Length - 4; i = i + 3)
                                {
                                    brokenBString[i] = brokenBString[i].Replace('.', ',');
                                    brokenBString[i + 1] = brokenBString[i + 1].Replace('.', ',');
                                    brokenBString[i + 2] = brokenBString[i + 2].Replace('.', ',');
                                    mesh[id].normals[vn] = new Vector3(float.Parse(brokenBString[i]), float.Parse(brokenBString[i + 1]),
                                                                float.Parse(brokenBString[i + 2]));
                                    vn++;
                                }

                                count++;
                            }
                            else
                            {
                                for (int i = 4; i < brokenBString.Length-4; i=i+2)
                                {
                                    brokenBString[i] = brokenBString[i].Replace('.', ',');
                                    brokenBString[i + 1] = brokenBString[i + 1].Replace('.', ',');
                                    mesh[id].uv[vt] = new Vector2(float.Parse(brokenBString[i]), float.Parse(brokenBString[i + 1]));
                                    vt++;
                                }
                            }
                        }
                        break;
                    case "triangles":
                        if (onlyOnce)
                        {
                            currentText = reader.ReadLine();
                            currentText = currentText.Trim();
                            brokenBrokenString = currentText.Split(' ');
                            while (!brokenBrokenString[0].Contains("<p>"))
                            {
                                currentText = reader.ReadLine();
                                currentText = currentText.Trim();
                                brokenBrokenString = currentText.Split(' ');
                            }
                            brokenBrokenString[0] = brokenBrokenString[0].Remove(0, 3);

                            for (int i = 0; i < brokenBrokenString.Length - 3; i = i + 3)
                            {
                                //Debug.Log(brokenBrokenString[i] + " " + brokenBrokenString[i + 1] + " " + brokenBrokenString[i + 2]);
                                //Debug.Log(brokenBrokenString[brokenBrokenString.Length-1] + " " + mesh[id].faceData.Length);
                                mesh[id].faceData[f2] = new Vector3(System.Convert.ToInt32(brokenBrokenString[i]), System.Convert.ToInt32(brokenBrokenString[i + 1])
                                    , System.Convert.ToInt32(brokenBrokenString[i + 2]));
                                mesh[id].triangles[f] = System.Convert.ToInt32(brokenBrokenString[i]);
                                f++;
                                f2++;
                            }
                            onlyOnce = false;
                        }
                        break;
                    default:
                        break;
                }
                currentText = reader.ReadLine();
            }
        }
    }
    
    private static bonesStruct createBoneStruct(string filename, GameObject go)
    {
        bonesStruct res;
        res = new bonesStruct();
        int nbJoint = 0;
        bool onlyOnce = false;
        List<float> weights = new List<float>();
        StreamReader stream = File.OpenText(filename);
        string entireText = stream.ReadToEnd();
        stream.Close();
        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();
            char[] splitIdentifier = { ' ' };
            string[] brokenString;
            while (!currentText.StartsWith("<library_controllers>"))
            {
                currentText = reader.ReadLine();
            }
            while (!currentText.StartsWith("</library_controllers>"))
            {
                currentText = currentText.Trim();                           //Trim the current line
                brokenString = currentText.Split(splitIdentifier);
                switch (brokenString[0])
                {
                    case "<Name_array":
                        string[] brokenBString;
                        brokenBString = currentText.Split(splitIdentifier);
                        nbJoint = brokenBString.Length - 2;
                        res.bones = new Transform[nbJoint-1];
                        res.bonesName = new string[nbJoint-1];
                        res.bindPoses = new Matrix4x4[nbJoint-1];
                        brokenBString[2] = brokenBString[2].Remove(0, brokenBString[2].IndexOf('>') + 1);
                        int k = 2;
                        for (int i = 0; i < res.bonesName.Length; i++)
                        {
                            res.bonesName[i] = brokenBString[k];
                            k++;
                        }
                        break;
                    case "<float_array":
                        if (!onlyOnce)
                        {
                            onlyOnce = true;
                        }
                        else
                        {
                            brokenString[2] = brokenString[2].Remove(0, brokenString[2].IndexOf('>')+1);
                            for (int i = 2; i < brokenString.Length-2; i++)
                            {
                                brokenString[i] = brokenString[i].Replace('.', ',');
                                weights.Add(float.Parse(brokenString[i].Trim()));
                            }
                        }
                        break;
                    case "<vertex_weights":
                        currentText = reader.ReadLine();
                        currentText = currentText.Trim();
                        string[] brokenBBString;
                        brokenBBString = currentText.Split(splitIdentifier);
                        while (!brokenBBString[0].Contains("<vcount>"))
                        {
                            currentText = reader.ReadLine();
                            currentText = currentText.Trim();
                            brokenBBString = currentText.Split(splitIdentifier);
                        }
                        currentText = currentText.Trim();
                        brokenBBString = currentText.Split(splitIdentifier);
                        brokenBBString[0] = brokenBBString[0].Remove(0, brokenBBString[0].IndexOf('>') + 1);
                        List<int> temp = new List<int>();
                        for (int i = 0; i < brokenBBString.Length-2; i++)
                        {
                            temp.Add(int.Parse(brokenBBString[i]));
                        }
                        currentText = reader.ReadLine();
                        currentText = currentText.Trim();
                        brokenBBString = currentText.Split(splitIdentifier);
                        res.weights = new BoneWeight[temp.Count+1];
                        brokenBBString[0] = brokenBBString[0].Remove(0, brokenBBString[0].IndexOf('>') + 1);
                        Debug.Log(brokenBBString[0]);
                        int j = 0;
                        for (int i = 0; i < temp.Count; i++)
                        {
                            res.weights[i].boneIndex0 = int.Parse(brokenBBString[j]);
                            j++;
                            res.weights[i].weight0 = weights[int.Parse(brokenBBString[j])];
                            j++;
                            if (temp[i] == 2)
                            {
                                res.weights[i].boneIndex1 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight1 = weights[int.Parse(brokenBBString[j])];
                                j++;
                            }
                            else if(temp[i] == 3)
                            {
                                res.weights[i].boneIndex1 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight1 = weights[int.Parse(brokenBBString[j])];
                                j++;
                                res.weights[i].boneIndex2 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight2 = weights[int.Parse(brokenBBString[j])];
                                j++;
                            }
                            else
                            {
                                res.weights[i].boneIndex1 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight1 = weights[int.Parse(brokenBBString[j])];
                                j++;
                                res.weights[i].boneIndex2 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight2 = weights[int.Parse(brokenBBString[j])];
                                j++;
                                res.weights[i].boneIndex3 = int.Parse(brokenBBString[j]);
                                j++;
                                res.weights[i].weight3 = weights[int.Parse(brokenBBString[j])];
                                j++;
                                j = j + ((temp[i] * 2) - 8);
                            }
                        }
                        
                        break;
                    default:
                        break;
                }
                currentText = reader.ReadLine();
            }
        }
        res.bones = makeJoint(res.bonesName, res.bones, entireText, go, res.bindPoses);
        return res;
    }

    private static Transform[] makeJoint(string[] s, Transform[] transforms, string entireText, GameObject go, Matrix4x4[] bindPoses)
    {
        Stack<Transform> parents = new Stack<Transform>();
        parents.Push(go.transform);
        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();
            char[] splitIdentifier = { ' ' };
            string[] brokenString;
            while (!currentText.StartsWith("<library_visual_scenes>"))
            {
                currentText = reader.ReadLine();
            }
            while (!currentText.StartsWith("</library_visual_scenes>"))
            {
                currentText = currentText.Trim();                           //Trim the current line
                brokenString = currentText.Split(splitIdentifier);
                switch (brokenString[0])
                {
                    case "<node":
                        if (!brokenString[3].Equals("type=\"NODE\">")){
                            brokenString[1] = brokenString[1].Remove(0, brokenString[1].IndexOf('=')+1);
                            brokenString[1] = brokenString[1].Replace("\"","");
                            int index = 0;
                            for (int i = 0; i < s.Length; i++)
                            {
                                if (s[i].Equals(brokenString[1]))
                                {
                                    index = i;
                                    break;
                                }
                            }
                            transforms[index] = new GameObject(s[index]).transform;
                            if (parents.Count < 1)
                            {
                                //bindPoses[index] = transforms[index].worldToLocalMatrix * go.transform.localToWorldMatrix;
                                parents.Push(transforms[index]);
                                transforms[index].parent = parents.Peek();
                            }
                            else
                            {
                                transforms[index].parent = parents.Peek();
                                //bindPoses[index] = transforms[index].worldToLocalMatrix * go.transform.localToWorldMatrix;
                                parents.Push(transforms[index]);
                            }
                            transforms[index].localRotation = Quaternion.identity;
                            currentText = reader.ReadLine();
                            currentText = currentText.Trim();
                            brokenString = currentText.Split(splitIdentifier);
                            brokenString[1] = brokenString[1].Remove(0, brokenString[1].IndexOf('>')+1);
                            brokenString[3] = brokenString[3].Remove(brokenString[3].IndexOf('<'));
                            brokenString[1] = brokenString[1].Replace('.', ',');
                            brokenString[2] = brokenString[2].Replace('.', ',');
                            brokenString[3] = brokenString[3].Replace('.', ',');
                            transforms[index].localPosition = new Vector3(float.Parse(brokenString[1]), float.Parse(brokenString[2]), float.Parse(brokenString[3]));
                        }
                        break;
                    case "</node>":
                        if(parents.Count != 0)
                        {
                            parents.Pop();
                        }
                        break;
                    default:
                        break;
                }
                currentText = reader.ReadLine();
            }
        }
        return transforms;
    }